
import StateEstimation
import StateEstimation.StateEstimator2D

import qualified Data.Time.Clock as Clock
import Control.Concurrent (threadDelay)
import qualified Data.Matrix as Matrix
import qualified Data.Vector as Vector

main :: IO ()
main = do
  currentTime <- Clock.getCurrentTime
  threadDelay 1000000
  newTime <- Clock.getCurrentTime

  let currentState = State { mean = Vector.fromList [1, 1, 1, 1],
                             covar = Matrix.diagonalList 4 0 [1,1,1,1],
                             time  = currentTime
                           }

  let newState     = State { mean = Vector.fromList [2, 2, 1, 1],
                             covar = Matrix.diagonalList 4 0 [1,1,1,1],
                             time  = newTime
                           }

  print ( "Initial State" )
  print ( currentState )

  print ( "Observed State" )
  print ( newState )

  print ( "Predicted State (from initial to time of observation)" )
  print ( predict currentState newState )

  print ( "Filtered State" )
  print ( estimate currentState newState )
