module MHT
  (
    HypothesisTree
  ) where

import qualified Data.Vector as Vector
import qualified Data.Matrix as Matrix
import StateEstimation (State,
                        Observation,
                        state,
                        covar,
                        kalman,
                        constantVelocityPredictionModel2d
                        )

data Tree a = Leaf a | Node a (Tree a) (Tree a) deriving (Show)
instance Functor Tree where
  fmap f (Node val left right) = Node (f val) (fmap f left) (fmap f right)
  fmap f (Leaf val) = Leaf (f val)

type HypothesisTree a = Tree (State a)




stateEstimator :: (Eq a, Fractional a) => State a -> Observation a -> State a
stateEstimator = kalman constantVelocityPredictionModel2d

spatialGate :: (Eq a, Fractional a, Floating a, Ord a) => State a -> Observation a -> Bool
spatialGate current observation = matrixSum < 10 -- Arbitrary number. FIXME
  where
    matrixSum = foldl (+) 0 $ mahalanobisDistance current observation

mahalanobisDistance :: (Eq a, Fractional a, Floating a) => State a -> Observation a -> Matrix.Matrix a
mahalanobisDistance current observation =
  fmap sqrt ((Matrix.transpose diff) * covariance * diff)
  where
    diff = Matrix.colVector $ Vector.zipWith (-) (state observation) (state current)
    covariance = case (Matrix.inverse (covar current)) of
                   Left  s -> error s   -- Throw error if Matrix inverse failed. FIXME
                   Right m -> m

addObservation :: (Eq a, Fractional a, Floating a, Ord a) => Observation a -> State a -> State a
addObservation observation current = if spatialGate current observation
  then stateEstimator current observation
  else current

mht :: (Eq a, Fractional a, Floating a, Ord a) => HypothesisTree a -> Observation a -> HypothesisTree a
mht tree observation = fmap (addObservation observation) tree
