module StateEstimation
    ( State(State),
      Observation,
      mean,
      covar,
      time,
    ) where


import Data.Matrix (Matrix)
import Data.Vector (Vector)
import Data.Time.Clock (UTCTime)

-- |The 'State' datatype contains the state of a system at a single point in time.
-- States are described as a Gaussian blob (similar to 'StateEstimation.Filters.Kalman.Distribution')
-- along with an associated point in time.
--
-- See 'StateEstimation.Filters.Kalman.Distribution' for more information.
data State a = State { mean::Vector a, covar::Matrix a, time::UTCTime }
  deriving (Show, Read)

type Observation a = State a

-- |A 'Track' is updated with either an incoming 'Observation', or by nothing.
-- If a track is not updated with an 'Observation' then we'll need the time
-- for which the update is occuring so that we can predict it forward in time.
data TrackUpdate a = Observation a | CoastingTrack UTCTime

-- |A 'Track' is defined simply as the pedigree of its 'State's, plus the updates
-- which triggered each. The 'TrackUpdate' is kept with each 'State' so that we can
-- deal with out-of-order measurements; for example if an 'Observation' comes in
-- with a time before the most recent state estimate then the update can be propogated
-- through the track states at the point of the 'Observation'.
type Track a = [(State a, TrackUpdate a)]
