-- |Contains a Kalman filter for filtering error and noise from a series of measurements.
module StateEstimation.Filters.Kalman
    ( Distribution(Distribution),
      mean,
      covar,
      predict,
      update
    ) where

import Data.Vector (Vector)
import Data.Matrix (Matrix, transpose, inverse, colVector, getMatrixAsVector)

-- |A Gaussian distribution centered at 'mean' with errors defined by 'covar'
data Distribution a = Distribution { mean :: Vector a, covar :: Matrix a}
  deriving (Show, Read)

-- |The 'predict' function uses a transformation function to predict the next state of a system.
--
-- The state of the system is defined using Gaussian blobs which are represented with
-- n-dimensional matrices. Each Gaussian is represented with the 'Distribution' data type,
-- which is defined with a mean and a covariance matrix. Combined, this gives us information
-- about what the state of the system most likely is (the mean), as well as what errors we
-- expect to have in that measurement (the covariance).
--
-- The prediction functions for the mean and covariances are as follows:
--
--   - \(x_k = F_k x_{k-1} + B_k u_k\)
--   - \(P_k = F_k P_{k-1} F_k^T + Q_k\)
--
-- Where:
--
--   - \(x_k\): The mean of the state of the system at discrete time \(k\)
--   - \(P_k\): The covariance of the state of the system at discrete time \(k\)
--   - \(F_k\): The transition function which defines how the current state vector is predicted forward
--   - \(Q_k\): Additional errors expected to be introduced by the environment
--   - \(B_k\): The transition function which defines how a separate control signal is transformed into the same form as the state vector
--   - \(u_k\): A separate control signal (if any) which was measured
--
-- The arguments to this function are as follows:
--
--   - transition \(F_k\): The transition function to apply to the previous state of the system.
--     If given the identity matrix, the state will not be predicted forward (unless provided
--     with a control signal or noise)
--   - controlTransform \(B_k\): The transformation function to apply to the control signal
--   - previous \(x_{k-1}\): The previous state of the system
--   - control \(u_k\): The control signal measured from the environment. If given a zero matrix,
--     then no control signal will be added to the prediction.
--   - noise \(Q_k\): The additional noise (measured or expected) from the environment. If given
--     a zero matrix, then no noise will be added to the prediction.
predict :: (Num a) => Matrix a -> Matrix a -> Distribution a -> Vector a -> Matrix a -> Distribution a
predict transition controlTransform previous control noise =
  Distribution {
  mean  = getMatrixAsVector $ (_Fk * xk_1) + (_Bk * uk),
  covar = (_Fk * _Pk_1 * _FkT) + _Qk
  }
  where
    _Fk   = transition
    _FkT  = transpose transition
    _Bk   = controlTransform
    _Qk   = noise
    _Pk_1 = covar previous
    xk_1  = colVector (mean previous)
    uk    = colVector control

-- |The 'update' function updates our current estimate given how accurate our prediction
-- from the 'predict' function is.
--
-- The state of the system is defined using Gaussian blobs which are represented with
-- n-dimensional matrices. Each Gaussian is represented with the 'Distribution' data type,
-- which is defined with a mean and a covariance matrix. Combined, this gives us information
-- about what the state of the system most likely is (the mean), as well as what errors we
-- expect to have in that measurement (the covariance).
--
-- The update functions for the mean and covariances are as follows:
--
--   - \(x'_k = x_k + K( z_k - H_k x_k )\)
--   - \(P'_k = P_k - K( H_k P_k )\)
--   - \(K = \frac{P_k H_k^T}{H_k P_k H_k^T + R_k} = P_k H_k^T (H_k P_k H_k^T + R_k)^{−1}\)
--
-- Where:
--
--   - \(x_k\): The mean of the state of the system at discrete time \(k\)
--   - \(P_k\): The covariance of the state of the system at discrete time \(k\)
--   - \(z_k\): The mean of the new measurement that was taken at discrete time \(k\)
--   - \(R_k\): The covariance of the new measurement that was taken at discrete time \(k\)
--   - \(H_k\): The "observation model" which is used to convert the state vector into the
--     same structure as the (potentially indirect) sensor readings. For example, imagine a
--     scenario in which you have a GPS sensor reading your position as well as a separate
--     sensor which measures velocity; you could exclude the velocity information when
--     updating the state with the position information and vice-versa. When given an
--     identity matrix, the function will assume that the measurement is provided in the
--     same form as the state vector (eg. [latitude, longitude] -> [latitude, longitude]).
--   - \(K\):   The Kalman gain of the system, effectively how much we /weight/ our new
--     observation versus how much we weight the accuracy of our model  when we're updating
--     our current estimate of the state of the system. We can tell from the equation that
--     it is a ratio of the covariance of the current prediction's errors to the total
--     combined error of the prediction and the new measurement.
update  :: (Eq a, Fractional a) => Matrix a -> Distribution a -> Distribution a -> Distribution a
update transition prediction measurement =
  Distribution {
  mean  = getMatrixAsVector $ xk + (_K * (zk - (_Hk * xk))),
  covar = _Pk - (_K * _Hk * _Pk)
  }
  where
    _Hk  = transition
    _HkT = transpose transition
    _Pk  = covar prediction
    _Rk  = covar measurement
    xk   = colVector (mean prediction)
    zk   = colVector (mean measurement)
    
    _K_denominator = case inverse ((_Hk * _Pk * _HkT) + _Rk) of
      Left  s -> error s   -- Throw error if Matrix inverse failed. FIXME
      Right m -> m

    _K = _Pk * _HkT * _K_denominator
