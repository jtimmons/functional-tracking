module StateEstimation.StateEstimator2D
    ( predict,
      update,
      estimate
    ) where

import StateEstimation
import qualified StateEstimation.Filters.Kalman as Kalman

import qualified Data.Time.Clock as Clock
import qualified Data.Matrix as Matrix
import qualified Data.Vector as Vector


stateToDist :: State a -> Kalman.Distribution a
stateToDist (State mean covar _) = Kalman.Distribution mean covar

distToState :: Kalman.Distribution a -> Clock.UTCTime -> State a
distToState (Kalman.Distribution mean covar) time = State mean covar time

predict :: (Fractional a) => State a -> Observation a -> State a
predict current measurement =
  distToState
  (Kalman.predict transition zeroMatrix (stateToDist current) zeroVector zeroMatrix)
  (time measurement)
  where
    t = realToFrac $ Clock.diffUTCTime (time measurement) (time current)
    zeroMatrix = Matrix.zero 4 4
    zeroVector = Vector.fromList [0,0,0,0]    
    transition = Matrix.fromLists [[1,0,t,0],
                                   [0,1,0,t],
                                   [0,0,1,0],
                                   [0,0,0,1]]

-- update function updates the estimated state (post-prediction) based on a measurement
update  :: (Eq a, Fractional a) => State a -> Observation a -> State a
update prediction measurement =
  distToState
  (Kalman.update (Matrix.identity 4) (stateToDist prediction) (stateToDist measurement))
  (time measurement)

estimate  :: (Eq a, Fractional a) => State a -> Observation a -> State a
estimate current measurement = update (predict current measurement) measurement
