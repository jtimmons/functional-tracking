#+STARTUP: showeverything

* Overview
This project is an attempt to experiment with a functional style of functional
state estimation and object tracking. The main intuition here is that conventional
estimation of an object's state through the use of a Kalman filter can be very
naturally conceptualized as an accumulation of new state measurements into an
existing state estimate, and that the Kalman filter itself can be very naturally
expressed as a composition of it's predict and update steps.

At a more practical level, this means that the Kalman filter (or in the case of this
project, a slightly limited sub-type of it) is extremely well suited for application
in a functional setting.

** The Plan

*State Estimation*
  - Create constant-velocity prediction model (assuming a four-dimensional state)
  - Implement Kalman filter update loop. Dimensionality shouldn't matter here as long
    as it's consistent between received states
  - Composite predict and update steps into 'kalman' function
  - Structure 'kalman' function as an accumulator of observations into states, so that
    we can fold that function over a list of observations to create a state estimate
    for that observation based on the state pedigree


In the future:
  - Provide some sort of higher-order function which can take in a prediction function
    and return a Kalman filter function using that prediction scheme
  - Consider creating statically typed-states so that runtime errors in linear algebra
    is impossible

*Tracking*
  - When an observation is received, map it over the Hypothesis tree, applying state
    estimation only to nodes which meet particular gating criteria.
