module Main where

import StateEstimation.StateEstimator2D

main :: IO ()
main = do
  initialState <- readLn
  input <- getContents -- stdin

  let observations = map read $ lines input
  print ( foldl estimate initialState observations )
